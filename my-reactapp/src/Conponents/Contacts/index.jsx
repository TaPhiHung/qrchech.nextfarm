import React from 'react';
import { Element } from 'react-scroll';
import PopupEmail from '../PopupEmail';

function Contacts(props) {
    return (
        <Element name="#contacts">
            <section className="flex justify-center blcok" id="contacts">
                <div className="relative">
                    <div className="absolute right-0 left-0 top-0 ">
                        <div className="">
                            {/* <div className="">
                                <div className="flex justify-center text-white">
                                    <div>
                                        <h2 className="flex justify-center text-2xl uppercase py-4">Liên hệ</h2>
                                        <p className="flex justify-center text-gray-400">Liên hệ với chúng tôi để biết thêm chi tiết về sản phẩm này</p>
                                    </div>
                                </div>
                                <div className="flex justify-evenly pb-10 xl:text-black 2xl:pt-10 xl:pt-20 xl:pb-4 sm:pt-1 pt-10 sm:text-white text-white">
                                    <div className="">
                                        <div className="flex justify-center text-2xl py-4 xl:text-blue-900 sm:text-white text-white">
                                            <i className="fa fa-envelope" />
                                        </div>
                                        <p>E-mail</p>
                                    </div>
                                    <div className="">
                                        <div className="flex justify-center text-2xl py-4 xl:text-blue-900 sm:text-white text-white">
                                            <i className="fa fa-phone" />
                                        </div>
                                        <p>Số điện thoại</p>
                                    </div>
                                    <div className="">
                                        <div className="flex justify-center text-2xl py-4 xl:text-blue-900 sm:text-white text-white">
                                            <i className="fa fa-map-marker" />
                                        </div>
                                        <p>Địa chỉ</p>
                                    </div>
                                </div>
                            </div> */}
                            <PopupEmail triggers={() =>
                                <div className="justify-center flex 2xl:mt-48 xl:mt-36  md:mt-32 sm:mt-20 mt-12">
                                    <button className="border-2 border-white bg-indigo-600 hover:bg-indigo-800 text-white px-16 py-2 text-xl font-semibold" > Liên hệ </button>
                                </div>
                            } />
                        </div>
                    </div>
                    <div className="z-10">
                        <div className="bg-indigo-800 2xl:pt-10 xl:pt-16 lg:pt-20 md:pt-20 sm:pt-20 pt-20">
                            <img src="https://qrcheck.nextfarm.vn/images/bg-shape-light.png" alt="" />
                        </div>
                    </div>
                </div>
            </section >
        </Element>
    );
}

export default Contacts;