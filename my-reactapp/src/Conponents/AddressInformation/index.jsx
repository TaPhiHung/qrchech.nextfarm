import React, { useState } from "react";
import { useForm } from "react-hook-form";
import axios from 'axios';

function AddressIngormation() {
    const { errors, register, handleSubmit } = useForm({
        criteriaMode: "all"
    });
    const [count, setCount] = useState({ name: "", email: "", details: "", title: "" });

    const handleChange = event => {
        const name = event.target.name;
        setCount({
            ...count,
            [name]: event.target.value
        });
    };
    const onSubmit = event => {

        const dataForm = {
            name: count.name,
            email: count.email,
            details: count.details,
            title: count.title
        };
        console.log(dataForm);
        axios.post(`https://api.nextfarm.vn/api/public/season/contact/pr=AS122`, { dataForm })
            .then(res => {
                console.log(res);
            })
    };

    return (
        <>
            <div className="rounded shadow-2xl">
                <div className="flex flex-row-reverse text-2xl pr-4 pt-2">
                    <button >
                        <i className="fas fa-times"></i>
                    </button>
                </div>
                <div className="bg-white lg:px-16 md:px-12 sm:px-8 shadow-md py-6">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="">
                            <div className="grid lg:grid-cols-2 sm:grid-cods-1 gap-x-6">
                                <div className="grid grid-rows-3">
                                    <div className="flex flex-wrap content-end pb-1">
                                        <label htmlFor="name">Tên(<span className="text-red-500">*</span>)</label>
                                    </div>
                                    <input className=" border px-3 py-2" name="name" type="text" placeholder="Nhập họ tên..."
                                        onChange={handleChange}
                                        ref={register({
                                            required: true
                                        })} />
                                    {errors.name && <p className="text-red-500 text-sm" >Họ tên chưa được nhập</p>}
                                </div>
                                <div className="grid grid-rows-3">
                                    <div className="flex flex-wrap content-end pb-2">
                                        <label htmlFor="email">Địa chỉ Email(<span className="text-red-500">*</span>)</label>
                                    </div>
                                    <input className="border px-3 py-2" name="email" type="email" placeholder="Nhập địa chỉ email..."
                                        onChange={handleChange}
                                        ref={register({
                                            required: true
                                        })} />
                                    {errors.email && <p className="text-red-500 text-sm" >Email chưa được nhập</p>}
                                </div>
                            </div>

                            <div className="grid grid-rows-3 justify-items-stretch">
                                <div className="flex flex-wrap content-end pb-2">
                                    <label htmlFor="title">Tiêu đề(<span className="text-red-500">*</span>)</label>
                                </div>
                                <input className="border px-3 py-2 block" name="title" type="text" placeholder="Nhập tiêu đề..."
                                    onChange={handleChange}
                                    ref={register({
                                        required: true
                                    })} />
                                {errors.title && <p className="text-red-500 text-sm" >Tiêu đề chưa được nhập</p>}
                            </div>

                            <div className="grid justify-items-stretch">
                                <div className="flex flex-wrap content-end pb-2">
                                    <label htmlFor="details">Nội dung(<span className="text-red-500">*</span>)</label>
                                </div>
                                <textarea name="details" className="border px-3 py-2 justify-self-auto" placeholder="Nhập nội dung..." rows="4" onChange={handleChange} ref={register} />
                            </div>
                        </div>
                        <div className="flex justify-end pt-3">
                            <button type="submit" className="border py-2 px-3 bg-red-400 hover:bg-red-500 text-white">Gửi thông tin</button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}
export default AddressIngormation;