import React from 'react';

function Distributor(props) {
    return (
        <section className="bg-gray-100">
            <div className="pb-28">
                <div className="flex justify-center py-6 text-gray-500 text-2xl uppercase">
                    <h2>Nhà phân khối</h2>
                </div>
                <div className="flex justify-center text-gray-400">
                    <p>Danh sách các nhà phân khối sản phẩm</p>
                </div>
            </div>
        </section >
    );
}

export default Distributor;