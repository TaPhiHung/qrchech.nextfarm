import React, { useState } from 'react';
import { Link } from 'react-scroll';
import PopupEmail from '../PopupEmail/';
import { IconContext } from 'react-icons';
import * as AiIcons from 'react-icons/ai';
import * as FaIcons from 'react-icons/fa';
import { SidebarData } from './SidebarData';
import './Navbar.css';
import './dialogForm.css';
// import SnackbarProvider from 'react-simple-snackbar'

import Dialog from 'react-dialog';
// import 'react-dialog/css/index.css';

function Header(props) {
    const [sidebar, setSidebar] = useState(false);
    const showSidebar = () => setSidebar(!sidebar);
    const qr = props.qr;

    const [dialog, setDialog] = useState(false);
    const opentDialog = () => setDialog(true);
    const handleDialog = () => setDialog(false);

    const callbackDialog = (close) => {
        setDialog(close)
    };
    return (
        <>
            <IconContext.Provider value={{ color: '#000' }}>
                <div className="bg-white shadow-md">
                    <div className="navbar md:container m-auto">
                        <Link to="#">
                            <img className="pl-10 h-8 w-auto sm:h-10 flex justify-start 2xl:pl-28 pl-1" src="https://img.nextfarm.vn/hosco/1564628506646/logo-big-white.png" alt=""></img>
                        </Link>
                        <Link to="#" className='menu-bars md:hidden'>
                            <FaIcons.FaBars onClick={showSidebar} />
                        </Link>
                        <nav className="hidden md:flex space-x-10 2xl:pl-64 xl:pl-32 px-1 justify-end flex-grow lg:mr-24">
                            <ul className="flex p-4 text-sm text-gray-500">
                                <li className="px-4"><Link activeClass="active" to="#home" spy={true} smooth={true} duration={500} >Sản phẩm</Link></li>
                                <li className="px-4"><Link activeClass="active" to="#company" spy={true} smooth={true} duration={500} >Công ty</Link></li>
                                <li className="px-4"><Link activeClass="active" to="#product" spy={true} smooth={true} duration={500} >Thông tin sản phẩm</Link></li>
                                <li className="px-4"><Link to="#" onClick={opentDialog}>Liên hệ</Link></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                {dialog &&
                    <Dialog
                        modal
                        onClose={handleDialog}
                        height={null}
                        width={null}>
                        <PopupEmail parentCallback={callbackDialog} qr={qr} />
                    </Dialog>
                }

                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className="nav-menu-items shadow-md ">
                        <li className="navbar-toggle flex">
                            <Link to="#" className="menu-bars">
                                <AiIcons.AiOutlineClose onClick={showSidebar} />
                            </Link>
                        </li>
                        {SidebarData.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path} spy={item.spy} smooth={item.smooth} duration={item.duration} onClick={showSidebar}>
                                        {item.icon}
                                        <span onClick={item.dialog ? opentDialog : null}>{item.title}</span>
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                </nav>
            </IconContext.Provider>
        </>
    );
}
export default Header;