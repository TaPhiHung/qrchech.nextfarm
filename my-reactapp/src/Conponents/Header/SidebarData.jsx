export const SidebarData = [
    {
        title: "Sản phẩm",
        path: "#home",
        cName: 'nav-text',
        spy: true,
        smooth: true,
        duration: 500,
        dialog: false
    },
    {
        title: "Công ty",
        path: "#company",
        cName: 'nav-text',
        spy: true,
        smooth: true,
        duration: 500,
        dialog: false
    },
    {
        title: "Thông tin sản phẩm",
        path: "#product",
        cName: 'nav-text',
        spy: true,
        smooth: true,
        duration: 500,
        dialog: false
    },
    {
        title: "Liên hệ",
        path: "#",
        cName: 'nav-text',
        spy: true,
        smooth: true,
        duration: 500,
        dialog: true
    },
]