import React from 'react';

function Footer(props) {
    return (
        <div className="shadow-md bg-gray-800">
            <div className="container mx-auto">
                <div className="xl:ml-28 lg:ml-20 md:12 sm:ml-8 flex items-center py-5 text-base ml-6">
                    <strong className="text-blue-600 pr-1">Nextfarm</strong>
                    <p className="text-gray-400">@ 2019 All Rights Reserved</p>
                </div>
            </div>
        </div>
    );
}

export default Footer;