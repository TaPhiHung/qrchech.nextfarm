import React from 'react';
import { Link } from "react-router-dom";

function Manufact(props) {
    return (
        <section >
            <div className="pl-2 container mx-auto 2xl:px-28 pt-8 xl:px-24 lg:px-20 md:px-16 sm:px-8 mb-12">
                <div className="border-b-2 flex justify-between">
                    <div className="py-4">
                        <h2 className="uppercase font-semibold text-gray-500">Đơn vị sản xuất</h2>
                    </div>
                    <div className="flex items-center ">
                        <div className="text-white flex text-sm flex justify-end">
                            <div className="bg-green-400 flex-initial px-2 py-1 mr-2">
                                <Link to="#">Liên hệ</Link>
                                <i className="fa fa-angle-down pl-1" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section >
    );
}

export default Manufact;