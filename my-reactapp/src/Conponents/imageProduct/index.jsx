import React from 'react';
import { Link } from "react-router-dom";

function ImageProduct(props) {
    return (
        <section>
            <div className="pl-2 container mx-auto 2xl:px-28 pt-8 xl:px-24 lg:px-20 md:px-16 sm:px-8">
                <div className="border-b-2 py-4">
                    <h2 className="uppercase font-semibold text-gray-500">Hình ảnh sản phẩm và quá trình sản xuất</h2>
                </div>
                <div>
                    <img src="" alt="" />
                </div>
                <div>
                    <div className="py-4 text-blue-500 text-sm">
                        <Link to="#">Xem thêm</Link>
                    </div>
                </div>
            </div>
        </section >
    );
}

export default ImageProduct;