import Header from './../Header';
import Home from './../Home';
import Company from './../Company';
import Product from './../Product';
import Footer from '../Footer';
import React from 'react';
import { useQuery } from "react-query";
import './Load.css';


function CallAPI(props) {

    const { qr } = props;
    var url = `http://api.nextfarm.vn/api/public/season/v2/${qr}`;
    const { isLoading, error, data } = useQuery("repoData", async () =>
        fetch(url).then((res) => res.json())
    );
    if (isLoading) return (
        <div className="loader">
            <span className="fas fa-spinner xoay iconOne"></span>
        </div>
    );

    // if (isFetching) return <h1>Update...</h1>;

    // if (isSuccess) return <h1 className="text-center text-xl py-16">Error</h1>;

    if (error) return "An error has occurred: " + error.message;

    return (
        <>
            <React.StrictMode>
                <div className="sticky top-0 z-40">
                    <Header qr={qr} />
                </div>
                <div className="top-0">
                    <Home dataHome={data.data} />
                </div>

                <Company dataCompany={data.data} />

                <Product dataProduct={data.data} />

                <div className="z-30">
                    <Footer />
                </div>
            </React.StrictMode>
        </>
    );
}

export default CallAPI;
