import React from 'react';
import { Element } from 'react-scroll';

function Company(props) {
    const { status_code } = props.dataCompany.info;
    var nameCompany = "";
    var addressCompany = "";
    var telCompany = "";
    var emailCompany = "";
    var websiteCompany = "";

    if (status_code === 1) {
        const { name, address, tel, email, website } = props.dataCompany.tenant;
        nameCompany = name;
        addressCompany = address;
        telCompany = tel;
        emailCompany = email;
        websiteCompany = website;
    }

    return (
        <Element name="#company">
            {status_code === 1 ?
                <section>
                    <div className="pl-2 md:container mx-auto 2xl:px-28 pt-8 xl:px-24 lg:px-20 md:px-16 sm:px-8 pb-8">
                        <div className="border-b-2 flex justify-between">
                            <div className="py-4 px-5">
                                <h2 className="uppercase font-semibold text-gray-500">Doanh nghiệp sở hữu</h2>
                            </div>
                        </div>
                        <div className="pt-6 px-5">
                            <div className="">
                                <h2 className="uppercase font-semibold">{nameCompany}</h2>
                                <p><i className="fa fa-map-marker pr-4" /><span className="font-semibold pr-2">Địa chỉ:</span> {addressCompany}</p>
                                <p><i className="fa fa-phone pr-3" /><span className="font-semibold pr-2">Điện thoại:</span> {telCompany}</p>
                                <p><i className="fa fa-envelope pr-2" /><span className="font-semibold pr-2">Email:</span> {emailCompany}</p>
                                <p><i className="fa fa-globe pr-3" /><span className="font-semibold pr-2">Website:</span> {websiteCompany}</p>
                            </div>
                        </div>
                    </div>
                </section > :
                <div></div>
            }
        </Element>
    )
}
export default Company;