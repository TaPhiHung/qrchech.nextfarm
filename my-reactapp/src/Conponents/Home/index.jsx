import React from 'react';
import { Element } from 'react-scroll';

function Home(props) {
    const { img_status, title, message, status_code } = props.dataHome.info;

    var cropName = "";
    var cropCode = "";
    var endDate = "";
    var startDate = "";

    if (status_code === 1) {
        const { start_date, end_date, crop_name, crop_code } = props.dataHome.season;
        cropName = crop_name;
        cropCode = crop_code;
        endDate = end_date;
        startDate = start_date;
    }

    return (
        <Element name="#home">
            <section >
                <div className="md:container mx-auto py-6">
                    <div>
                        <div className="flex justify-center">
                            <img src={img_status} alt="" />
                        </div>
                        <div className="flex justify-center">
                            <div className="text-sm text-green-500">
                                <p className="text-center font-medium">{title}</p>
                                <p className="text-center">{message}</p>
                            </div>
                        </div>
                    </div>
                    {status_code === 1 ?
                        <div className="2xl:px-28 pt-8 xl:px-24 lg:px-20 md:px-16 sm:px-8 px-5">
                            <div className="pl-2">
                                <h3 className="py-2 text-gray-500"><strong>Tên sản phẩm : {cropName}</strong></h3>
                                <p>Mã SP: <span className="text-gray-500">{cropCode}</span></p>
                                <p>Ngày sản xuất: <span className="text-gray-500">{startDate}</span></p>
                                <p>Ngày thu hoạch: <span className="text-gray-500">{endDate}</span></p>
                            </div>
                        </div>
                        :
                        <div className="h-48"></div>
                    }
                </div>
            </section >
        </Element>
    );
}

export default Home;