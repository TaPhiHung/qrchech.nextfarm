import React from 'react';

function Production(props) {
    return (
        <section className="bg-white">
            <div className="pb-28">
                <div className="flex justify-center py-6 text-gray-500 text-2xl ">
                    <h2>Quá trình sản xuất</h2>
                </div>
                <div className="flex justify-center text-gray-400">
                    <p>Thông tin các giai đoạn sản xuất</p>
                </div>
            </div>
        </section >
    );
}

export default Production;