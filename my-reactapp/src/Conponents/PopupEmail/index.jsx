import React, { useState } from 'react';
import { useForm } from "react-hook-form";
import axios from 'axios';
import toastr from 'toastr';
// import '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css';
function PopupEmail(props) {

    const closeDialog = () => {
        props.parentCallback(false);
    }
    const qr = props.qr;

    const { errors, register, handleSubmit } = useForm({
        criteriaMode: "all"
    });
    const [count, setCount] = useState({ name: "", phone: "", details: "", title: "" });

    const handleChange = event => {
        const name = event.target.name;
        setCount({
            ...count,
            [name]: event.target.value
        });
    };

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-full-width",
        "preventDuplicates": false,
        "onclick": null,
    }
    const onSubmit = event => {
        let url = `https://api.nextfarm.vn/api/public/season/contact/${qr}`
        axios.post(url, {
            name: count.name,
            phone: count.phone,
            details: count.details,
            title: count.title
        })
            .then(res => {
                console.log(res);
                const success = res.data.meta.status_code;
                const message = res.data.meta.message;
                if (success === 0) {
                    closeDialog();
                    toastr.success(message);

                } else if (success === 1) {
                    toastr.error(message);
                }
            })
            .catch(error => console.log(error));
    };

    const fontFamily = {
        "fontFamily": "Redressed",
        "fonrFamily": "cursive"
    };
    return (
        <>
            <div className="bg-white chieurong sm:w-96 bg-white rounded-lg">
                <div className="flex justify-center mb-2 sm:px-10 px-6">
                    <img src="https://img.nextfarm.vn/hosco/1564628506646/logo-big-white.png" alt="" />
                </div>
                <div className="flex justify-center bg-green-500 text-white py-2 lg:text-3xl sm:text-2xl text-xl">
                    <h2 style={fontFamily}>Thông tin liên hệ</h2>
                </div>
                <div className="sm:px-5 px-3 py-1 lg:text-base sm:text-md text-sm" >
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <div className="flow-root grid grid-rows-2">
                                <label htmlFor="name">Tên(<span className="text-red-500 ml-0">*</span>)</label>
                                <input type="text"
                                    className="rounded border border-gray-300 hover:border-gray-600 px-3 py-1 transition delay-100"
                                    placeholder="Nhập họ tên..." onChange={handleChange} name="name"
                                    ref={register({
                                        required: true
                                    })} />
                                {errors.name && <p className="text-red-500 md:text-sm text-xs" >Họ tên chưa được nhập</p>}
                            </div>
                        </div>
                        <div className="lg:pt-4 sm:pt-2 pt-1">
                            <div className="flow-root grid grid-rows-2">
                                <label htmlFor="phone">Số điện thoại(<span className="text-red-500 ml-0">*</span>)</label>
                                <input type="number"
                                    className="rounded border border-gray-300 hover:border-gray-600 px-3 py-1 transition delay-100"
                                    placeholder="Nhập số điện thoại..." onChange={handleChange} name="phone"
                                    ref={register({
                                        required: true,
                                        pattern: /((09|03|07|08|05)+([0-9]{8})\b)/g,
                                    })} />
                                {errors.phone && <p className="text-red-500 md:text-sm text-xs" >Điện thoại chưa được nhập</p>}
                            </div>
                        </div>
                        <div className="lg:pt-4 sm:pt-2 pt-1">
                            <div className="flow-root grid grid-rows-2">
                                <label htmlFor="title">Tiêu đề(<span className="text-red-500 ml-0">*</span>)</label>
                                <input type="text"
                                    className="rounded border border-gray-300 hover:border-gray-600 px-3 py-1 transition delay-100"
                                    placeholder="Nhập tiêu đề..." onChange={handleChange} name="title"
                                    ref={register({
                                        required: true
                                    })} />
                                {errors.title && <p className="text-red-500 md:text-sm text-xs" >Tiêu đề chưa được nhập</p>}
                            </div>
                        </div>
                        <div className="lg:pt-4 sm:pt-2 pt-1">
                            <div className="flow-root grid grid-rows-4">
                                <label htmlFor="details">Nội dung(<span className="text-red-500 ml-0">*</span>)</label>
                                <textarea name="details" rows="4"
                                    className="rounded border border-gray-300 hover:border-gray-600 px-3 py-1 row-span-3 transition delay-100"
                                    placeholder="Nhập nội dung..." onChange={handleChange} ref={register} />
                            </div>
                        </div>
                        <div className="flex justify-center hover:shadow-lg">
                            <button
                                className="block mt-4 py-2 bg-red-400 hover:bg-red-600 text-white w-full transition delay-100 rounded-md text-center ">
                                Gửi thông tin
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}
export default PopupEmail;