import React from 'react';
import { Element } from 'react-scroll';
import './../Product/product.css';

function Product(props) {
    const { status_code } = props.dataProduct.info;

    var fullDescription = "";
    if (status_code === 1) {
        const { full_description } = props.dataProduct.season;
        fullDescription = full_description;
    }
    return (
        <Element name="#product">{
            status_code === 1 ?
                <section className="bg-white block pb-12" id="product">
                    <div className="md:container mx-auto 2xl:px-28 xl:px-4">
                        <div className="bg-white">
                            <div className="px-5">
                                <div className="text-justify" dangerouslySetInnerHTML={{ __html: fullDescription }} /> :
                            </div>
                        </div>
                    </div>
                </section > :

                <div></div>
        }
        </Element>
    );
}

export default Product;