import React from 'react';

function BuyProduct(props) {
    return (
        <section >
            <div className="pl-2 container mx-auto 2xl:px-28 pt-8 xl:px-24 lg:px-20 md:px-16 sm:px-8 ">
                <div className="flex justify-center bg-green-400 hover:bg-green-500 text-while text-white">
                    <div className="py-2 flex justify-center">
                        <i className="fa fa-shopping-cart margin-right-10 px-3"></i>
                        <h3 className="text-sm">Mua sản phẩm</h3>
                    </div>
                </div>
            </div>
        </section >
    );
}

export default BuyProduct;