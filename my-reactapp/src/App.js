import React from "react";
import {
  BrowserRouter as Router
} from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import CallAPI from './Conponents/CallAPI';

const queryClient = new QueryClient();

function App() {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString).get('qr');

  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <CallAPI qr={urlParams} />
      </Router>
    </QueryClientProvider>
  );
}

export default App;